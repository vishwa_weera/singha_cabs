<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'contact_no'=>'required',
            'password' => 'required|min:6|confirmed',
        ]);
    }


    protected function create(array $data)
    {
        $user = User::create([
                  'name' => $data['name'],
                  'email' => $data['email'],
                  'contact_no' => $data['contact_no'],
                  'password' => bcrypt($data['password']),
        ]);

        //email to new user
        sendEmail($data);
        return $user;
    }
}


function sendEmail($data)
{
    Mail::send('emails.sucess_registration', ['data' =>$data], function($message) use ($data) {
        $message
            ->to($data['email'])
            ->subject('Registration');
    });

}
