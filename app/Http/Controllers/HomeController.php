<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Reservation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->user_type == 'user') {

            return view('home');
        }
        elseif(Auth::user()->user_type == 'admin') {

            $reservations = Reservation::all();


            return view('home',compact('reservations'));

        }

    }

    public function submitReservation(Requests\ReservationRequest $request)
    {
        $form = new Reservation;

        $form ->vehicle_type= $request->vehicle_type;
        $form ->date= $request->start_date;
        $form ->time= $request->time;
        $form ->start_point= $request->start_point;
        $form ->destination= $request->endpoint;
        $form ->no_of_passengers= $request->no_passengers;
        $form ->details_of_journey= $request->details;
        $form ->user_id= $request->logged_user_id;
        $form ->user_name= $request->logged_user_name;
        $form ->user_tel= $request->logged_user_tel;
        $form ->user_email= $request->logged_user_email;

        //reservation email
        $user_data['email'] = $request->logged_user_email;
        $user_data['name'] = $request->logged_user_name;
        $user_data['vehicle_type'] = $request->vehicle_type;
        $user_data['date'] = $request->start_date;
        $user_data['time'] = $request->time;
        $user_data['start_point'] = $request->start_point;
        $user_data['destination'] = $request->endpoint;
        $user_data['no_of_passengers'] = $request->no_passengers;
        $user_data['details_of_journey'] = $request->details;
        $this->sendReserveEmailToUser($user_data);
        $this->sendReserveEmailToAdmin($user_data);

        $form -> save();

        // Reservation success message
        Session::flash('msg_title','Reservation Done!');
        Session::flash('msg_type','successreservation');
        Session::flash('msg_text', 'The reservation was successfully recorded.');

        return redirect('/');
    }

    public function sendReserveEmailToUser($user_data)
    {
        Mail::send('emails.reservation', ['data' => $user_data], function ($message) use ($user_data) {
            $message
                ->to($user_data['email'])
                ->subject('Vehicle Reservation Confirmation');
        });

    }

    public function sendReserveEmailToAdmin($user_data)
    {
        Mail::send('emails.admin', ['data' => $user_data], function ($message) use ($user_data) {
            $message
                ->to('polsoma@gmail.com')
                ->subject('Vehicle Reservation');
        });

    }
}
