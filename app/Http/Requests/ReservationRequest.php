<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReservationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_type'=>'required',
            'start_date'=>'required',
            'time'=>'required',
            'start_point'=>'required',
            'endpoint'=>'required',
            'no_passengers'=>'required',

        ];
    }

    public function messages()
    {
        return [
            'vehicle_type.required'=>'Select a vehicle type',
            'start_date.required'=>'Start date required',
            'time.required'=>'Time of departure required',
            'start_point.required'=>'Starting location required',
            'endpoint.required'=>'Destination required',
            'no_passengers.required'=>'Number of passengers required',

        ];
    }
}
