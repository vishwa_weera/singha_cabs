@extends('layouts.app')

@section('content')

{{-- SWEET ALERT MESSAGES --}}
<script>
        $(document).on("ready", function(){
                    @if (Session::has('msg_text') && Session::get('msg_type')=='register')
            var title = "{{ Session::get('msg_title') }}";
            var text = "{{ Session::get('msg_text') }}";
            swal(title, text, "success");
            @elseif(Session::has('msg_text') && Session::get('msg_type')=='successreservation')
            var title = "{{ Session::get('msg_title') }}";
            var text = "{{ Session::get('msg_text') }}";
            swal(title, text, "success");
            @endif
        });
</script>

<div id="tooplate_wrapper">
    <div id="tooplate_main">
        <div class="col_w960">
            <div id="sliderFrame">
                <div id="slider">
                    <img src="jsImgSlider/images/1.jpg" />
                    <img src="jsImgSlider/images/2.jpg" />
                    <img src="jsImgSlider/images/3.jpg" />
                    <img src="jsImgSlider/images/4.jpg" />
                    <img src="jsImgSlider/images/5.png" />
                    <img src="jsImgSlider/images/6.png" />
                </div>
            </div>
            <div class="cleaner"></div>
        </div>

        <div class="col_w960 col_last">
            <div class="col_allw300 frontpage_box">
                <img src="images/52bcb6fe75304b9193ce40fc767f2254-emirates-a380-920a.jpg" height="50px" width="70px" alt="Image" />
                <h2>AIRPORT TRANSFERS!</h2>
                <span class="tagline">Fly without Stress</span>
                <p>We offer a prompt reliable airport transfer service covering all the major airports in Sri lanka. We can help take the hassle out of travel with our professional meet and greet service. One of our fully licensed drivers will be waiting for you at your designated airport to take you on your onward journey.</p>

            </div>

            <div class="col_allw300 frontpage_box">
                <img src="images/tooplate_bc_green.png" height="50px" width="70px" alt="Image" />
                <h2>SINGHA CABS! </h2>
                <span class="tagline">Anytime Anywhere</span>
                <p>We are 24/7 standby with more than 56 vehicles. Including budget cars and luxury models for you. Also Vans with different number of seating capabilities and much more! Just visit Availability Page to check availability and place order. Then we will assist you with the process.</p>

            </div>

            <div class="col_allw300 frontpage_box col_last">
                <img src="images/tooplate_bc_orange.png" height="50px" width="70px" alt="Image" />
                <h2>KEEP YOUR GATE OPEN! </h2>
                <span class="tagline">High quality service </span>
                <p>Once you Confirm your order Singha Cabs will be at your door step with Keys along with requested vehicle! Our drivers are trained to maintain passenger safety and help you with loading unloading as well. Our vehicles are well maintained to ensure your safety. </p>

            </div>
            <div class="cleaner"></div>
        </div>
    </div>
</div> <!-- end of wrapper -->



@endsection
