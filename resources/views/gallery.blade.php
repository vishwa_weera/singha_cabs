@extends('layouts.app')

@section('content')

<div id="tooplate_middle">
    <div id="mid_title">
        Our Gallery
    </div>

</div> <!-- end of middle -->


<div id="sliderFrame">
    <div id="slider">
        <a href="#" target="_blank">
            <img src="jsImgSlider/images/image-slider-1.jpg"  />
        </a>
        <img src="jsImgSlider/images/image-slider-2.jpg" alt="" />
        <img src="jsImgSlider/images/image-slider-3.jpg" alt="" />
        <img src="jsImgSlider/images/image-slider-4.jpg" alt="" />
        <img src="jsImgSlider/images/image-slider-5.jpg" alt=""/>
    </div>
    <div id="htmlcaption" style="display: none;">
        <em>HTML</em> caption. Link to <a href="http://www.google.com/">Google</a>.
    </div>
</div>

@endsection
