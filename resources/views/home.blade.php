@extends('layouts.app')

@section('content')

    dd();

@if(Auth::user()->user_type == 'user')

    <div id="tooplate_main">
        <div class="col_w900">

            <div class="col_w450 float_l">
                <h3>Quick Reservation Form</h3>
                <div id="cp_contact_form">
                    <form  method="POST" action="submitform" id="frm-reg">
                        <input type="hidden"  value="{{ csrf_token() }}" name="_token" id="_token">
                        {!! csrf_field() !!}
                        <input type="hidden" value="{{Auth::user()->id}}" name="logged_user_id" >
                        <input type="hidden" value="{{Auth::user()->name}}" name="logged_user_name" >
                        <input type="hidden" value="{{Auth::user()->contact_no}}" name="logged_user_tel" >
                        <input type="hidden" value="{{Auth::user()->email}}" name="logged_user_email" >
                        <ul>
                            <li><label>Vehicle Type:</label>
                                <select name="vehicle_type">
                                    <option value="" disabled selected>SELECT</option>
                                    <option value="car">Car</option>
                                    <option value="van">Van</option>
                                    <option value="nanocab">Nano Cab</option></select>
                            </li>
                            @if ($errors->has('vehicle_type'))
                                <span class="help-block" id="error-font">{{ $errors->first('vehicle_type') }}</span>
                            @endif
                            <li><label>Date:</label><input type="date" name="start_date" > </li>
                            @if ($errors->has('start_date'))
                                <span class="help-block" id="error-font">{{ $errors->first('start_date') }}</span>
                            @endif
                            <li><label>Time:</label><input type="time" name="time" > </li>
                            @if ($errors->has('time'))
                                <span class="help-block" id="error-font">{{ $errors->first('time') }}</span>
                            @endif
                            <li><label>Starting Point:</label><input type="text" name="start_point" > </li>
                            @if ($errors->has('start_point'))
                                <span class="help-block" id="error-font">{{ $errors->first('start_point') }}</span>
                            @endif
                            <li><label>Destination:</label><input type="text" name="endpoint" > </li>
                            @if ($errors->has('endpoint'))
                                <span class="help-block" id="error-font">{{ $errors->first('endpoint') }}</span>
                            @endif
                            <li><label>Num of Passengers:</label><input type="number" name="no_passengers" > </li>
                            @if ($errors->has('no_passengers'))
                                <span class="help-block" id="error-font">{{ $errors->first('no_passengers') }}</span>
                            @endif
                            <li><label>Details of Journey:</label><textarea cols="4" rows="5" name="details"></textarea> </li>
                            <li> <label><input type="submit" Value = "Submit"></label></li>
                        </ul>

                    </form>

                </div>

            </div>


            <div class="cleaner"></div>
        </div>

    </div> <!-- end of main -->

@elseif(Auth::user()->user_type == 'admin')

    <div class="col-sm-10 col-sm-offset-1 table-pad-fr">
        <p></p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>RESERVATION ID</th>
                <th>VEHICLE TYPE</th>
                <th>DATE</th>
                <th>TIME</th>
                <th>START POINT</th>
                <th>DESTINATION</th>
                <th>NO OF PASSNAGERS</th>
                <th>DETAILS OF JOURNEY</th>
                <th>NAME</th>
                <th>TELEPHONE</th>
                <th>EMAIL</th>

                {{--<th>SATUTS</th>--}}
            </tr>
            </thead>

            @foreach($reservations as $reservation)

                <tbody>
                <tr>
                    <td>{{$reservation->id}}</td>
                    <td>{{$reservation->vehicle_type}}</td>
                    <td>{{$reservation->date}}</td>
                    <td>{{$reservation->time}}</td>
                    <td>{{$reservation->start_point}}</td>
                    <td>{{$reservation->destination}}</td>
                    <td>{{$reservation->no_of_passengers}}</td>
                    <td>{{$reservation->details_of_journey}}</td>
                    <td>{{$reservation->user_name}}</td>
                    <td>{{$reservation->user_tel}}</td>
                    <td>{{$reservation->user_email}}</td>


                </tr>
                </tbody>

            @endforeach


        </table>

    </div>

@endif


@endsection

