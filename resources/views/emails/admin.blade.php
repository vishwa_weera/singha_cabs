<h1>Reservation</h1>
<p>You have another vehicle reservation. Wake up!!!!!</p>
<p><b>Name : {{$data['name']}}</b></p>
<p><b>Email : {{$data['email']}}</b></p>
<p><b>Vehicle type : {{$data['vehicle_type']}}</b></p>
<p><b>Starting date : {{$data['date']}}</b></p>
<p><b>Time : {{$data['time']}}</b></p>
<p><b>Start point : {{$data['start_point']}}</b></p>
<p><b>Destination : {{$data['destination']}}</b></p>
<p><b>Number of passengers : {{$data['no_of_passengers']}}</b></p>
<p><b>Details of journey : {{$data['details_of_journey']}}</b></p>