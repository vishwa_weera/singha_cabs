<h1>Confirmation</h1>
<p>Your reservation was successfully recorded as shown below. Our service will be provided to you as needed.
    If there are any changes to be made, please contact us. <br> Thank you.</p>
<p><b>Name : {{$data['name']}}</b></p>
<p><b>Vehicle type : {{$data['vehicle_type']}}</b></p>
<p><b>Starting date : {{$data['date']}}</b></p>
<p><b>Time : {{$data['time']}}</b></p>
<p><b>Start point : {{$data['start_point']}}</b></p>
<p><b>Destination : {{$data['destination']}}</b></p>
<p><b>Number of passengers : {{$data['no_of_passengers']}}</b></p>
<p><b>Details of journey : {{$data['details_of_journey']}}</b></p>