@extends('layouts.app')

@section('content')



    <div id="tooplate_middle">
    <div id="mid_title">
        Contact Information
    </div>
    <p> Contact us for more details.</p>
</div> <!-- end of middle -->

<div id="tooplate_main">
    <div class="col_w900">

        <div class="col_w450 float_l">
            <h3>Quick Contact Form</h3>
            <div id="cp_contact_form">
                <form method="post" name="contact" action="#">

                    <label for="author">Name:</label>
                    <input type="text" id="author" name="author" class="required input_field" />
                    <div class="cleaner h10"></div>

                    <label for="email">Email:</label>
                    <input type="text" class="validate-email required input_field" name="email" id="email" />
                    <div class="cleaner h10"></div>

                    <label for="subject">Subject:</label>
                    <input type="text" class="validate-subject required input_field" name="subject" id="subject"/>
                    <div class="cleaner h10"></div>

                    <label for="text">Message:</label>
                    <textarea id="text" name="text" rows="0" cols="0" class="required"></textarea>
                    <div class="cleaner h10"></div>

                    <input type="submit" value="Send" id="submit" name="submit" class="submit_btn float_l" />


                </form>

            </div>

        </div>

        <div class="col_w450 float_r">


            <div class="cleaner h30"></div>

            <h3>Our Address</h3>
            <h6><strong>Singha Cabs (PVT) Ltd.</strong></h6>
            No129/7, <br />
            Hospital Road,Horana<br /><br />
            Tel: 011-2xxxxxx<br />
            Mobile: 071-xxxxxxx<br />
        </div>

        <div class="cleaner"></div>
    </div>

</div> <!-- end of main -->

</div> <!-- end of wrapper -->


@endsection