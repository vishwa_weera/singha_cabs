@extends('layouts.app')

@section('content')


<div id="tooplate_wrapper">
    <div id="tooplate_main">
        <div class="col_w960">

            <div class="col_w450 float_l">
                <h2>We are...</h2>
                <img class="image_wrapper image_fl" src="images/tooplate_image_05.jpg" height="200px" width="200px" alt="Image 05" />
                <p>Singha Cabs Founded in year 2004 with total vehicle count of 23. We were at the bottom of the ladder by then. As we develop a firm base with our clients our prime goal was to increase the number of vehicles. For a starter like that was a huge task with pricings of new vehicles and managing resources for customer satisfaction. Mr. Saman Kumara who worked around the clock with a firm mind set and unshaken targets brought a new era.</p>
                <em style="text-align=center"><strong>Safety is our Concern</strong>.</em>

                <p>Today, we do possess over 56 vehicles including luxury cars and providing services to our customers with greater satisfaction. Now we are in the process of digitalizing our services so that we can extend our path closer to our customers. The launch of this official website is a yet another small step of that effort. As a company we believe that our customers earn the benefit of this website and are satisfied with our services.
                </p>

                <p>We value our customer feedback. Also we are open for any comments and suggestions. Contact us using any of the following methods. For detailed instructions on how to contact us, please visit Contact Us page.</p>
            </div>

            <div class="col_w450 float_r">

                <h2>Just some reasons why us !!</h2>
                <ul><li>All Major Cards Accepted Including American Express</li>
                    <li>Online Bookings</li>
                    <li> Receipt Available</li>
                    <li>Unmarked Vehicles</li>
                    <li> One Hour Waiting Time</li>
                    <li> Confirmation Emails</li>

                </ul>
            </div>

            <div class="cleaner"></div>
        </div>

    </div> <!-- end of main -->
</div>



@endsection