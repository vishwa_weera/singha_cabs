<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle_type',20);
            $table->string('date',20);
            $table->string('time',30);
            $table->string('start_point');
            $table->string('destination');
            $table->string('no_of_passengers',10);
            $table->string('details_of_journey')->nullable();
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('user_tel',20);
            $table->string('user_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
