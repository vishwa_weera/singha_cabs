/**
 * Created by SHALINDA on 12/23/2015.
 */

var app = angular.module('ifttt', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}).config(function ($sceProvider) {
    $sceProvider.enabled(false);
});

app.controller('iftttController', function ($http, $scope) {

    var mconfig=$http.get('js/mconnect/configs.json').success(function(data) {mconfig=data;});
    var auth_end=null;
    var client_id=null;
    var redirect_uri=null;
    var max_age = 3600;
    var acer_values = '2';
    var cusMobileNumber=null;
    var token=null;
    var is_logged=false;
    var accs_token=null;

    $scope.doAuthorize = function () {
        client_id=mconfig['client_id'];
        auth_end=mconfig['auth_end'];
        redirect_uri=mconfig['redirect_uri'];

        if(is_logged) {
            logOut();
            location.reload();
        }
        var response_type = "code";
        var state = 'State' + Math.random().toString(36);
        var nonce = 'Nonce' + Math.random().toString(36);
        var login_hint = null;
        var prompt = 'login';
        authorizationOptions = new AuthorizationOptions('page', 'en', 'en', 'Enter MSISDN', login_hint, null);
        authorize(auth_end, client_id, 'openid email profile phone', redirect_uri, response_type, state, nonce, prompt, max_age, acer_values, authorizationOptions, getToken);

    }

    function logOut(){
        var url = 'https://mconnect.dialog.lk/openidconnect/j_spring_security_logout?callback=?';

        $.ajax({
            type: 'GET',
            url: url,
            async: false,
            jsonpCallback: 'jsonCallback',
            contentType: "application/json",
            dataType: 'jsonp',
            success: function(json) {
                window.location.reload();
            },
            error: function(e) {

            }
        });
    }

    function getToken(data){
        $('#loading-title').html('Getting Ready...');
        $('#loading').show();


        code = data['code'];
        state = data['state'];
        error = data['error'];
        var client_secret = "k8BF5BrhGlRfswgLpQsiUlgnLrvFVNfq";
        var token_end = "https://mconnect.dialog.lk/openidconnect/token";
        if (code && code != null && (code.trim().length) > 0) {
            var token_end = "https://mconnect.dialog.lk/openidconnect/token";
            tokenFromAuthorizationCode(token_end, code, client_id, client_secret, redirect_uri, getInfo);
        } else {
            $('#loading-title').html('Login Failed,Please Try Again.');
            $('#loading').delay(2000).hide(1);
        }


    }


    function getInfo(latestToken) {
        session.setToken(latestToken.access_token);

        token=latestToken;

        if (!!token.access_token) {
            var info_end="https://mconnect.dialog.lk/openidconnect/userinfo";
            accs_token=token.access_token;
            userinfo(info_end, accs_token, userInfoReceived);
        }else{

        }

    }
    function userInfoReceived(user){
        $('#loading').hide();
        is_logged=true;
        cusMobileNumber=user.phone_number;
    }
});